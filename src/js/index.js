import $ from 'jquery'
window.jQuery = $;
window.$ = $;


import Swiper from 'swiper/bundle';
/*
ON LOAD PAGE FUNCTION
*/

jQuery( window ).on( 'load', function() {

    $('body').removeClass('is-load');

} );

/*
INITIALIZATION FUNCTIONS
*/

jQuery( document ).ready( function( $ ) {

    new Swiper('.featured--section__slider', {
        slidesPerView: 1,
        navigation: {
            nextEl: '.swiper-button-prev',
            prevEl: '.swiper-button-next',
        },
    });

    console.log(`Всего тегов :`  + document.getElementsByTagName('*').length)
    const tags = ['p', 'h2', 'div','img','svg',  'span' ,'main' ,'body' ,'html' , 'head', 'link', 'meta', 'title' ,'input' ,'label', 'script',   'section' ];
    tags.forEach(el => {
        console.log(`${el} : ` + document.querySelectorAll(el).length)
        console.log(`Количество символов в теге ${el.length}`)
    })
} );

/*
ON SCROLL PAGE FUNCTIONS
*/

jQuery( window ).on( 'scroll', function() {



} );